# Mentoringkeller Bot

Dieser Telegrambot zeigt die Anzahl der Geräte der Mentoringkeller WLAN APs an, der RWTH Aachen.

## Docker
### Official Build
- [hub.docker.com/r/bergiu/mentoringkellerbot](https://hub.docker.com/r/bergiu/mentoringkellerbot/)

### Build
- `docker build -t bergiu/mentoringkellerbot:v1.0.0 .`
- `docker build -t bergiu/mentoringkellerbot:latest .`

### Run
- `docker run --name mentoringkellerbot -e TOKEN=$DEINTELEGRAMTOKEN -d bergiu/mentoringkellerbot`

### Upload
- `docker tag bergiu/mentoringkellerbot bergiu/mentoringkellerbot`
- `docker push bergiu/mentoringkellerbot`
