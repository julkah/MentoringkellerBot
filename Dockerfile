FROM debian
MAINTAINER Marco Schlicht (marcoschlicht@onlinehome.de)

ENV DEBIAN_FRONTEND noninteractive

# install dependencies
RUN apt-get -qq update && apt-get -qq install python3 python3-pip && pip3 install bs4 && pip3 install python-telegram-bot

# copy bot
ADD bot /bot

# initial startup
WORKDIR /bot
ENTRYPOINT /bot/entrypoint.sh
CMD []
