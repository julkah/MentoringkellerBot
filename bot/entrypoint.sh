#!/bin/bash
BASEDIR=$(dirname "$0");
cd $BASEDIR;

if [[ -n "$TOKEN" ]];
then
	python3 main.py $TOKEN
else
	echo 'you must specify an env var $TOKEN that contains the token'
fi
